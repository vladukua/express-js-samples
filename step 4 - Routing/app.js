var express = require("express");
 
var app = express();
app.get("/", function(request, response){
     
    response.send("<h1>Main page</h1>");
});
// "/about"
app.get("/about", function(request, response){
     
    response.send("<h1>About web site</h1>");
});
// "/contact"
app.get("/contact", function(request, response){
     
    response.send("<h1>Contacts</h1>");
});
// /bk or /bok
app.get("/bo?k", function (request, response) {
  response.send("Route: /bo?k.  URL: "+  request.url);
});
// "/bok" , "/book", "/boook" ...
app.get("/bo+k", function (request, response) {
  response.send("Route: /bo+k.  URL: " + request.url)
});
// "/bork", "/bonk", "/bor.dak", "/bor/ok"
app.get("/bo*k", function (request, response) {
  response.send("Route: /bo*k.  URL: " + request.url)
});
// segments
// "/products/5", "/products/phone"
app.get("/products/:productId", function (request, response) {
  response.send("productId: " + request.params["productId"]); //request.params.productId
});
// "/categories/smartphone/products/iphone7"
app.get("/categories/:categoryId/products/:productId", function (request, response) {
    var catId = request.params["categoryId"];
    var prodId = request.params["productId"];
  response.send(`Category: ${catId}    Product: ${prodId}`);
});
// "/file/iphone7.php"
app.get("/file/:pageName.:pageExt", function (request, response) {
    var pageName = request.params["pageName"];
    var pageExt = request.params["pageExt"];
  response.send(`Requested file: ${pageName}.${pageExt}`);
});
app.listen(3000);