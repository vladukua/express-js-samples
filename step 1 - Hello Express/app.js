var express = require("express");
// Create Express instance
var app = express();
// Root route handler
app.get("/", function(request, response){      
    response.send("<h2>Hello Express!</h2>");
});
// starting server on 3000 port.
app.listen(3000);