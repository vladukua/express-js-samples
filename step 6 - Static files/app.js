var express = require("express");
 
var app = express();
 
app.use(express.static(__dirname + "/public"));
//app.use("/static", express.static(__dirname + "/public"));
 
app.get("/", function(request, response){
     
    response.send("<h1>Main Page</h1>");
});
app.get("/contact", function(request, response){
     
    response.send("<h1>Contacts</h1>");
});
app.listen(3000);