var express = require('express');
var app = express();
 
var myJSONObj = {
    "Hello" : "World"
}
 
app.get('/', function(req, res){
    // http://expressjs.com/en/api.html#res.format
    res.format({
        html: function(){
            res.send('<h1>Hello World</h2>');
        },
 
        text: function(){
            res.send('Hello World');
        },
 
        json: function(){
            res.json(myJSONObj);
        }
    })
});
 
app.listen(3000);
console.log('Listening on port 3000');