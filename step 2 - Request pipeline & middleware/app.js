var express = require("express");
 
var app = express();
app.use(function(request, response, next){
     
    console.log("Middleware 1");
    next();
});
app.use("/about", function(request, response, next){
     
    console.log("Middleware 2");
    next();
});
 
app.get("/", function(request, response, next){
    console.log("Route /");
    response.send("Hello");
    next();
});
app.use(function(request, response, next){
     
    console.log("Middleware 2");
    next();
});

app.listen(3000);