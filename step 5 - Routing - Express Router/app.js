var express = require("express");
 
var app = express();
 
var productRouter = express.Router();
productRouter.route("/")
            .get(function(request, response){
     
                response.send("<h2>Product List.</h2>");
            });
productRouter.route("/:id")
            .get(function(request, response){
     
                response.send(`<h2>Product ${request.params.id}</h2>`);
            });
app.use("/products", productRouter);
 
app.get("/", function(request, response){
     
    response.send("<h2>Main Page</h2>");
});
 
app.listen(3000);