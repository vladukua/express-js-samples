var express = require("express");
var bodyParser = require("body-parser");
 
var app = express();
// create parser for json data
var jsonParser = bodyParser.json();
 
app.use(express.static(__dirname + "/public"));
 
app.post("/user", jsonParser, function (request, response) {
    if(!request.body) return response.sendStatus(400);
    console.log(request.body);
    response.json(`${request.body.userName} - ${request.body.userAge}`);
});
 
app.get("/", function(request, response){
     
    response.send("<h1>Main Page</h1>");
});
 
app.listen(3000);