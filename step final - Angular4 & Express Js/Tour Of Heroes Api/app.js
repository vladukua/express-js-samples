var express = require("express");
var bodyParser = require("body-parser");
var fs = require("fs");
 
var app = express();
var jsonParser = bodyParser.json();

// Add headers
app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});
 
// get data from file
app.get("/api/heroes", function(req, res){
      
    var content = fs.readFileSync("heroes.json", "utf8");
    var heroes = JSON.parse(content);
    res.status(200).send({data: heroes});
});
// Get hero by id

app.get("/api/heroes/:id", function(req, res){
      
    var id = req.params.id; // get hero id from url
    var content = fs.readFileSync("heroes.json", "utf8");
    var heroes = JSON.parse(content);
    var hero = null;
    // find hero by id
    for(var i=0; i<heroes.length; i++){
        if(heroes[i].id==id){
            hero = heroes[i];
            break;
        }
    }
    // send data
    if(hero){
        res.status(200).send({data: hero});
    }
    else{
        res.status(404).send();
    }
});

app.delete("/api/heroes/:id", function(req, res) {
    var id = req.params.id;

    var content = fs.readFileSync("heroes.json", "utf8");
    var heroes = JSON.parse(content);
    var filteredHeroes = [];

    for(var i = 0; i < heroes.length; ++i) {
        if (heroes[i].id != id) {
            filteredHeroes.push(heroes[i]);
        }
    }
    if (filteredHeroes.length != heroes.length) {       
        fs.writeFileSync("heroes.json", JSON.stringify(filteredHeroes));

        res.status(200).send();
    } else {
        res.status(404).send();
    }
});

app.put("/api/heroes/:id", jsonParser, function(req, res) {
    var id = req.body.id;
    var name = req.body.name;

    var content = fs.readFileSync("heroes.json", "utf8");
    var heroes = JSON.parse(content);
    
    for(var i = 0; i < heroes.length; ++i) {
        if (heroes[i].id == id) {
            heroes[i].name = name;
            break;
        }
    }

    fs.writeFileSync("heroes.json", JSON.stringify(heroes));

    res.status(200).send();
});

app.post("/api/heroes", jsonParser, function(req, res) {
    var name = req.body.name;

    var content = fs.readFileSync("heroes.json", "utf8");
    var heroes = JSON.parse(content);

    var id = Math.max.apply(Math, heroes.map(function(o){
        return o.id;
    }));
    id++;

    if (name) {
        var content = fs.readFileSync("heroes.json", "utf8");
        var heroes = JSON.parse(content);
        var hero = {id: id, name: name};
        heroes.push(hero);

        fs.writeFileSync("heroes.json", JSON.stringify(heroes));

        res.status(200).send({data: hero});
    } else {
        res.status(500).send();
    }
});

app.get('/api/heroes/search/:term', function(req, res) {
    var term = req.params.term;
    var content = fs.readFileSync("heroes.json", "utf8");
    var heroes = JSON.parse(content);
    var filteredHeroes = [];

    for(var i = 0; i < heroes.length; ++i) {
        if (heroes[i].name.toLowerCase().indexOf(term.toLowerCase()) != -1) {
            filteredHeroes.push(heroes[i]);
        }
    }

    res.status(200).send({data: filteredHeroes});
});
  
app.listen(3002, function(){
    console.log("Waiting for connections...");
});